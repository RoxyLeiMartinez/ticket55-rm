#! /bin/bash

echo "Information request for dev-app-rm.procore.prod1 server per ticket54 as follows:" >> /tmp/serverinfo.info

echo "DATE of information request:" >> /tmp/serverinfo.info

date >> /tmp/serverinfo.info

echo "The LAST 10 USERS who logged onto this server are as follows:" >> /tmp/serverinfo.info

last -n 10 >> /tmp/serverinfo.info

echo "SWAP SPACE as follows:" >> /tmp/serverinfo.info

free -h >> /tmp/serverinfo.info


echo "OR another way to read swap space:" >> /tmp/serverinfo.info


free -m >> /tmp/serverinfo.info

echo "KERNAL VERSION is as follows:" >> /tmp/serverinfo.info

uname -r >> /tmp/serverinfo.info

echo "Ip Address is as follows:" >> /tmp/serverinfo.info

hostname -I >> /tmp/serverinfo.info



	 
